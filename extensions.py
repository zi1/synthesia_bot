import openai
from gtts import gTTS

def translate_text(text, lang):
    question="Translate this text "+text+" into " +lang
    openai.api_key = "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"
    response=openai.Completion.create(
    model="gpt-3.5-turbo-instruct",
    prompt=question,
    max_tokens=1024,
    temperature=0
    )
    answer=response["choices"][0]
    return answer

def text_to_speech(text):
    tts=gTTS(text=text, lang='en')
    filename= "speech.ogg"
    tts.save(filename)
    return filename