from __future__ import annotations

import logging
from telegram import Update, constants
from telegram import BotCommand
from telegram.ext import ApplicationBuilder, CommandHandler, MessageHandler, \
    filters, Application, ContextTypes

from bot.utils import get_thread_id, message_text, wrap_with_indicator, \
    get_reply_to_message_id, error_handler

from bot.synthesia_helper import SynthesiaHelper, localized_text
import requests

class SynthesiaTelegramBot:
    """
    Class representing a Synthesia Telegram Bot.
    """

    def __init__(self, config: dict, synthesia: SynthesiaHelper):
        """
        Initializes the bot with the given configuration and GPT bot object.
        :param config: A dictionary containing the bot configuration
        :param openai: OpenAIHelper object
        """
        self.config = config
        self.synthesia = synthesia
        bot_language = self.config['bot_language']
        self.commands = [
            BotCommand(command='start', description=localized_text('start_description', bot_language)),
        ]

        self.usage = {}
        self.last_message = {}
        self.inline_queries_cache = {}

    async def send_video(self, video_url: str, chat_id: str) -> None:

        url = f'https://api.telegram.org/bot{self.config["token"]}/sendVideoNote'

        payload = {
            "chat_id": chat_id,
            "video_note": video_url
        }
        headers = {
            "accept": "application/json",
            "User-Agent": "Telegram Bot SDK - (https://github.com/irazasyed/telegram-bot-sdk)",
            "content-type": "application/json"
        }

        response = requests.post(url, json=payload, headers=headers)

        print(response.text)

    async def start(self, update: Update, _) -> None:
        """
        Open main menu.
        """
        bot_language = update.message.from_user.language_code

        start_text = (
                f'{localized_text("hi", bot_language)} {update.message.from_user.name} 👋' +
                '\n' +
                localized_text('start_text', bot_language)
        )

        await update.message.reply_text(start_text)

    async def prompt(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        React to incoming messages and respond accordingly.
        """
        if update.edited_message or not update.message or update.message.via_bot:
            return

        logging.info(
            f'New message received from user {update.message.from_user.name} (id: {update.message.from_user.id})')
        prompt = message_text(update.message)
        bot_language = update.message.from_user.language_code
        chat_id = update.effective_chat.id


        try:
            async def _reply():
                await self.synthesia.create_video(text=prompt, callback_id=str(chat_id))

                await update.effective_message.reply_text(
                    message_thread_id=get_thread_id(update),
                    reply_to_message_id=get_reply_to_message_id(update),
                    text=localized_text('reply_text', bot_language),
                    parse_mode=constants.ParseMode.MARKDOWN
                )

            await wrap_with_indicator(update, context, _reply, constants.ChatAction.TYPING)

        except Exception as e:
            logging.exception(e)

    async def post_init(self, application: Application) -> None:
        """
        Post initialization hook for the bot.
        """
        await application.bot.set_my_commands(self.commands)

    def run(self):
        """
        Runs the bot indefinitely until the user presses Ctrl+C
        """
        application = ApplicationBuilder() \
            .token(self.config['token']) \
            .proxy_url(self.config['proxy']) \
            .get_updates_proxy_url(self.config['proxy']) \
            .post_init(self.post_init) \
            .concurrent_updates(True) \
            .build()

        application.add_handler(CommandHandler('start', self.start))
        application.add_handler(MessageHandler(filters.TEXT & (~filters.COMMAND), self.prompt))
        application.add_error_handler(error_handler)

        application.run_polling()
