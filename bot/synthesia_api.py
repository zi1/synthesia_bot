import requests


class SynthesiaAPI:
    def __init__(self, api_key, api_url='https://api.synthesia.io/v2/videos'):
        self.api_key = api_key
        self.api_url = api_url

    def request_video_task(self, text, callback_id):
        options = {
            'headers': {
                'Content-Type': 'application/json',
                'Authorization': self.api_key,
            },
            'json': {
                "test": True,
                "title": "Test",
                "description": "Test",
                "visibility": "public",
                "callbackId": callback_id,
                "input": [
                    {
                        "scriptText": text,
                        "avatar": "dbb1283d-2246-4aaa-b877-a6745475a85d",
                        "avatarSettings": {
                            "voice": "4657e005-b82e-4761-9272-2d9c24c61b61",
                            "horizontalAlign": "center",
                            "scale": 1.0,
                            "style": "rectangular"
                        },
                        "background": "luxury_lobby"
                    }
                ]
            }
        }
        response = requests.post(self.api_url, **options)
        return response.json()
