from flask import Flask, request, Response
from flask_cors import CORS
import logging
import asyncio
from bot.telegram_bot import SynthesiaTelegramBot


class FlaskHelper:
    """
    Class representing a Flask.
    """

    def __init__(self, config: dict, bot: SynthesiaTelegramBot):
        """
        Initializes the Flask with the given configuration
        :param config: A dictionary containing the flask configuration
        :param bot: Bot object
        """
        self.config = config
        self.bot = bot
        self.app = Flask(__name__)
        self.loop = asyncio.get_event_loop()
        CORS(self.app)

    def add_routes(self):
        @self.app.route("/video_completed", methods=['POST'])
        def video_completed():
            if request.method == "POST":
                try:
                    content = request.get_json()

                    url = content['data']['download']
                    chat_id = content['data']['callbackId']

                    self.loop.run_until_complete(
                        self.bot.send_video(url, chat_id)
                    )

                    return Response("{'success': 'true'}", status=400, mimetype='application/json')
                except Exception as e:
                    logging.exception(e)

    def run(self):
        """
        Runs the flask indefinitely until the user presses Ctrl+C
        """
        self.add_routes()

        self.app.run(host='0.0.0.0', port=self.config['port'], debug=True)
