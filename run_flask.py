import logging
import os

from dotenv import load_dotenv

from bot.synthesia_helper import SynthesiaHelper
from bot.telegram_bot import SynthesiaTelegramBot
from flask_helper import FlaskHelper


def main():
    # Read .env file
    load_dotenv()

    # Setup logging
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO
    )
    logging.getLogger("httpx").setLevel(logging.WARNING)

    # Check if the required environment variables are set
    required_values = ['TELEGRAM_BOT_TOKEN', 'SYNTHESIA_API_KEY']
    missing_values = [value for value in required_values if os.environ.get(value) is None]
    if len(missing_values) > 0:
        logging.error(f'The following environment values are missing in your .env: {", ".join(missing_values)}')
        exit(1)

    # Setup configurations
    synthesia_config = {
        'api_key': os.environ['SYNTHESIA_API_KEY'],
        'synthesia_avatar': os.environ['SYNTHESIA_AVATAR_ID'],
        'synthesia_template': os.environ['SYNTHESIA_TEMPLATE_ID'],
    }

    telegram_config = {
        'token': os.environ['TELEGRAM_BOT_TOKEN'],
        'bot_addr': os.environ['TELEGRAM_BOT_ADDR'],
        'proxy': os.environ.get('PROXY', None),
        'bot_language': os.environ.get('BOT_LANGUAGE', 'en'),
    }

    flask_config = {
        'port': int(os.environ.get('FLASK_PORT', 5000)),
        'telegram_bot_token': os.environ['TELEGRAM_BOT_TOKEN'],
    }

    synthesia_helper = SynthesiaHelper(config=synthesia_config)
    telegram_bot = SynthesiaTelegramBot(config=telegram_config, synthesia=synthesia_helper)
    flask_helper = FlaskHelper(config=flask_config, bot=telegram_bot)
    flask_helper.run()


if __name__ == '__main__':
    main()
